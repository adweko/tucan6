/*eslint no-console: 0*/
"use strict";

//imports
var http = require("http");
var port = process.env.PORT || 3000;

const server = require("http").createServer();
const express = require("express");
var path = require('path');


//Initialize Express App for XSA UAA and HDBEXT Middleware
const xsenv = require("@sap/xsenv");
const passport = require("passport");
const xssec = require("@sap/xssec");
const xsHDBConn = require("@sap/hdbext");
xsenv.loadEnv();

http.globalAgent.options.ca = xsenv.loadCertificates();
global.__base = __dirname + "/";
global.__uaa = process.env.UAA_SERVICE_NAME;

//connect express to Hana HDI container
const app = express();

//setup pug
app.set('views', './pug');
app.set('view engine', 'pug');

//setup body parser
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: false}));
app.use(bodyParser.json());

//Build a JWT Strategy from the bound UAA resource
passport.use("JWT", new xssec.JWTStrategy(xsenv.getServices({
	uaa: {
		tag: "xsuaa"
	}
}).uaa));
//Add Passport JWT processing
app.use(passport.initialize());

let hanaOptions = xsenv.getServices({
	hana: {
		plan: "hdi-shared"
	}
});

hanaOptions.hana.pooling = true;
//Add Passport for Authentication via JWT + HANA DB connection as Middleware in Expess
app.use(
	xsHDBConn.middleware(hanaOptions.hana),
	passport.authenticate("JWT", {
		session: false
	})
);


var DataFrame = require('dataframe-js').DataFrame;
var df = new DataFrame([
    {c1: 1, c2: 6}, // <------- A row
    {c4: 1, c3: 2}
], ['c1', 'c2', 'c3', 'c4']);

var df2 = new DataFrame([{c1: 1, c2: 6}, {c4: 1, c3: 2}], ['c1', 'c2', 'c3', 'c4']);


var appDir = path.dirname(require.main.filename);
var currentDirectory = path.join(__dirname, 'js', 'data/test.json');
console.log(currentDirectory);
console.log(appDir);
//let testFilePath = appDir + "/js/data/test.json";
//DataFrame.fromJSON(testFilePath).then(df3 => df3); 



//let json = require(currentDirectory);
let json = require('./data/test.json');
//console.log(json, 'the json obj'); //disabled verbose



console.log("Server listening on port %d", port);
console.log("Hello World12345");
console.log(df2.show());



//Hello Router
app.get("/sascha", (req, res) => {
		return res.type("text/plain").status(200).send("Hello World Node.js");
});


//setup additional Node.js routes
require("./router")(app, server);
server.on("request", app);
app.listen(port, () => console.log('Server started on port: ' + port));

//old maybe needed later:

/*
http.createServer(function (req, res) {
  res.writeHead(200, {"Content-Type": "text/plain"});
//  res.end("Hello World\n");
  res.end(df3.show());
}).listen(port);
*/

//var dust = require('dustjs-linkedin');
//dust.render();

//var conn = $.hdb.getConnection();
//var query = 'SELECT * FROM \"TUCAN6_HDI_DB_TUCAN_6_1\".\"MappingLayer\"';
//var resultSet = conn.executeQuery(query);
