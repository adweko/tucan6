/*eslint no-console: 0, no-unused-vars: 0, no-shadow: 0, new-cap: 0*/
/*eslint-env node, es6 */
"use strict";
var express = require("express");
var async = require("async");

module.exports = function() {
	var app = express.Router();
	
	
	//dummy sql query
		app.get("/", (req, res) => {
			let client = req.db;
			client.prepare(
				`SELECT SESSION_USER, CURRENT_SCHEMA
											 FROM "DUMMY"`,
				(err, statement) => {
					if (err) {
						return res.type("text/plain").status(500).send(`ERROR: ${err.toString()}`);
					}
					statement.exec([],
						(err, results) => {
							if (err) {
								return res.type("text/plain").status(500).send(`ERROR: ${err.toString()}`);
							} else {
								var result = JSON.stringify({
									Objects: results
								});
								return res.type("application/json").status(200).send(result);
							}
						});
					return null;
				});
			return null;
		});
	return app;
};