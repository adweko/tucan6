/*eslint no-console: 0, no-unused-vars: 0, no-shadow: 0, new-cap: 0*/
/*eslint-env node, es6 */
"use strict";
var express = require("express");
var async = require("async");

module.exports = function() {
	var app = express.Router();
	
	
	//SQL test 3= mapping layer
		app.get("/3", (req, res) => {
			let client = req.db;
			client.prepare(
				`SELECT * FROM \"TUCAN6_HDI_DB_TUCAN_6_1\".\"MappingLayer\";`,
				(err, statement) => {
					if (err) {
						return res.type("text/plain").status(500).send(`ERROR: ${err.toString()}`);
					}
					statement.exec([],
						(err, results) => {
							if (err) {
								return res.type("text/plain").status(500).send(`ERROR: ${err.toString()}`);
							} else {
								var result = JSON.stringify({
									Objects: results
								});
								return res.type("application/json").status(200).send(result);
							}
						});
					return null;
				});
			return null;
		});
		
		
		app.get("/2", (req, res) => {
			let client = req.db;
			client.prepare(
				`SELECT * FROM \"TUCAN6_HDI_DB_TUCAN_6_1\".\"JoinLayer\";`,
				(err, statement) => {
					if (err) {
						return res.type("text/plain").status(500).send(`ERROR: ${err.toString()}`);
					}
					statement.exec([],
						(err, results) => {
							if (err) {
								return res.type("text/plain").status(500).send(`ERROR: ${err.toString()}`);
							} else {
								var result = JSON.stringify({
									Objects: results
								});
								return res.type("application/json").status(200).send(result);
							}
						});
					return null;
				});
			return null;
		});
		
		
		app.get("/1", (req, res) => {
			let client = req.db;
			client.prepare(
				`SELECT * FROM \"TUCAN6_HDI_DB_TUCAN_6_1\".\"SourceTableLayer\";`,
				(err, statement) => {
					if (err) {
						return res.type("text/plain").status(500).send(`ERROR: ${err.toString()}`);
					}
					statement.exec([],
						(err, results) => {
							if (err) {
								return res.type("text/plain").status(500).send(`ERROR: ${err.toString()}`);
							} else {
								var result = JSON.stringify({
									Objects: results
								});
								return res.type("application/json").status(200).send(result);
							}
						});
					return null;
				});
			return null;
		});
		
		app.get("/df1", (req, res) => {
			let client = req.db;
			client.prepare(
				`SELECT * FROM \"TUCAN6_HDI_DB_TUCAN_6_1\".\"SourceTableLayer\";`,
				(err, statement) => {
					if (err) {
						return res.type("text/plain").status(500).send(`ERROR: ${err.toString()}`);
					}
					statement.exec([],
						(err, results) => {
							if (err) {
								return res.type("text/plain").status(500).send(`ERROR: ${err.toString()}`);
							} else {
								//var result = JSON.stringify({Objects: results});
								var DataFrame = require('dataframe-js').DataFrame;
								//var df = "[" + results + "]";
								var helperArray = results;
								var df = new DataFrame(helperArray);

								//var df2 = new DataFrame([{c1: 1, c2: 6}, {c4: 1, c3: 2}], ['c1', 'c2', 'c3', 'c4']);
								return res.type("application/json").status(200).send(df.show());
							}
						});
					return null;
				});
			return null;
		});
		
		
		
		
		
	return app;
};