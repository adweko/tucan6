/*eslint no-console: 0, no-unused-vars: 0, no-shadow: 0, new-cap: 0*/
/*eslint-env node, es6 */
"use strict";
var express = require("express");
var async = require("async");

var mappingLayer = "";


module.exports = function() {
	var app = express.Router();
	
	
	//Hallo Pug
	app.get("/", (req, res) => {
		res.render('index');
	});
	
	//Form 3 get
	app.get("/3", (req, res) => {
		res.render('form3');
	});
	
	//Form 3 post
	app.post("/form3sent", (req, res) => {
		console.log('Submitted');
		var ergebnisHTML = '<p>Daten erhalten. Vielen Dank</p><p>' + 
		'[1]= ' + req.body.dynamicMappingParameter1 +'</p><p>'+
		'[2]= ' + req.body.dynamicMappingParameter2 +'</p><p>'+
		'[3]= ' + req.body.dynamicMappingParameter3 +'</p>';
		
		var ergebnisString = 'Daten erhalten. Vielen Dank\r\n' + 
		'[1]= ' + req.body.dynamicMappingParameter1 +'\r\n'+
		'[2]= ' + req.body.dynamicMappingParameter2 +'\r\n'+
		'[3]= ' + req.body.dynamicMappingParameter3;
		
		ergebnisString = "test";
		
		ergebnisString = 'Daten erhalten. Vielen Dank' + 
		'[1]= ' + req.body.dynamicMappingParameter1 +''+
		'[2]= ' + req.body.dynamicMappingParameter2 +''+
		'[3]= ' + req.body.dynamicMappingParameter3;
		
		var rightNow = new Date();
		var resDate = rightNow.toISOString().slice(0,10).replace(/-/g,"");
		var resTime = rightNow.toLocaleTimeString().replace(/:/g,"").slice(0,5);
		var fileName = resDate + '_' + resTime + '.hdbprocedure';
		console.log(fileName);
		
		
		
		var htmlResult = `
	<script>
	function download(filename, text) {
    var pom = document.createElement('a');
    pom.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    pom.setAttribute('download', filename);

    if (document.createEvent) {
        var event = document.createEvent('MouseEvents');
        event.initEvent('click', true, true);
        pom.dispatchEvent(event);
    }
    else {
        pom.click();
    }
}` +

`download('`+ fileName +`', '`+ ergebnisString +`') </script>;` + ergebnisHTML;

		var excelJSON = JSON.parse(req.body.dynamicMappingParameter3);
		var resultsJoinLayer = excelJSON.JoinLayer;
		var resultsMappingLayer = excelJSON.MappingLayer;
		var resultsSourceTableLayer = excelJSON.SourceTableLayer;
		res.render('form3LandingSite.pug', {
								jsonJoinLayer: resultsJoinLayer,
								jsonMappingLayer: resultsMappingLayer,
								jsonSourceTableLayer: resultsSourceTableLayer
							});
		return;
	});
	
	//Form 2 get
	app.get("/2", (req, res) => {
		res.render('form2');
	});
	
	//Form 2 post
	app.post("/form2sent", (req, res) => {
		console.log('Submitted');
		var ergebnisHTML = '<p>Daten erhalten. Vielen Dank</p><p>' + 
		'[1]= ' + req.body.dynamicMappingParameter1 +'</p><p>'+
		'[2]= ' + req.body.dynamicMappingParameter2 +'</p><p>'+
		'[3]= ' + req.body.dynamicMappingParameter3 +'</p>';
		
		var ergebnisString = 'Daten erhalten. Vielen Dank\r\n' + 
		'[1]= ' + req.body.dynamicMappingParameter1 +'\r\n'+
		'[2]= ' + req.body.dynamicMappingParameter2 +'\r\n'+
		'[3]= ' + req.body.dynamicMappingParameter3;
		
		ergebnisString = "test";
		
		ergebnisString = 'Daten erhalten. Vielen Dank' + 
		'[1]= ' + req.body.dynamicMappingParameter1 +''+
		'[2]= ' + req.body.dynamicMappingParameter2 +''+
		'[3]= ' + req.body.dynamicMappingParameter3;
		
		var rightNow = new Date();
		var resDate = rightNow.toISOString().slice(0,10).replace(/-/g,"");
		var resTime = rightNow.toLocaleTimeString().replace(/:/g,"").slice(0,5);
		var fileName = resDate + '_' + resTime + '.hdbprocedure';
		console.log(fileName);
		
		
		
		var htmlResult = `
	<script>
	function download(filename, text) {
    var pom = document.createElement('a');
    pom.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    pom.setAttribute('download', filename);

    if (document.createEvent) {
        var event = document.createEvent('MouseEvents');
        event.initEvent('click', true, true);
        pom.dispatchEvent(event);
    }
    else {
        pom.click();
    }
}` +

`download('`+ fileName +`', '`+ ergebnisString +`') </script>;` + ergebnisHTML;
		res.send(ergebnisHTML);
		return;
	});
	
	//ExcelJS Test2
	app.get("/xls2", (req, res) => {
		res.render('exceljs2');
	});
	
	
	//ExcelJS Test
	app.get("/xls", (req, res) => {
		res.render('exceljs');
	});
	
	//SheetJS Test
	app.get("/sheetjs", (req, res) => {
		res.render('sheetjs');
	});
	
	
	//Variable test 6 - testing two queries
	app.get("/h2", async(req, res) => {
		try {
			const dbClass = require(global.__base + "utils/dbPromises");
			let db = new dbClass(req.db);
			
			//get JoinLayer
			const statementJoinLayer = await db.preparePromisified(`SELECT * FROM \"TUCAN6_HDI_DB_TUCAN_6_1\".\"JoinLayer\";`);
			const resultsJoinLayer = await db.statementExecPromisified(statementJoinLayer, []);
			
			//get MappingLayer
			const statementMappingLayer = await db.preparePromisified(`SELECT * FROM \"TUCAN6_HDI_DB_TUCAN_6_1\".\"MappingLayer\";`);
			const resultsMappingLayer = await db.statementExecPromisified(statementMappingLayer, []);
			
			
			//get SourceTableLayer
			const statementSourceTableLayer = await db.preparePromisified(`SELECT * FROM \"TUCAN6_HDI_DB_TUCAN_6_1\".\"SourceTableLayer\";`);
			const resultsSourceTableLayer = await db.statementExecPromisified(statementSourceTableLayer, []);
			
			
			//let result = JSON.stringify({Objects: results});
			return res.render('hdbfunction2.pug', {
								jsonJoinLayer: resultsJoinLayer,
								jsonMappingLayer: resultsMappingLayer,
								jsonSourceTableLayer: resultsSourceTableLayer
							});
		} catch (e) {
			return res.type("text/plain").status(500).send(`ERROR: ${e.toString()}`);
		}
	});
	
	
	
	
	
	
	
	//Variable 4 test
	app.get("/var4", (req, res) => {
		res.render('var4', {
			myVariable: 'Meine Variable'
		});
	});
	
	
	//Variable test 6 - testing two queries
	app.get("/var6", async(req, res) => {
		try {
			const dbClass = require(global.__base + "utils/dbPromises");
			let db = new dbClass(req.db);
			
			//get JoinLayer
			const statementJoinLayer = await db.preparePromisified(`SELECT * FROM \"TUCAN6_HDI_DB_TUCAN_6_1\".\"JoinLayer\";`);
			const resultsJoinLayer = await db.statementExecPromisified(statementJoinLayer, []);
			
			//get MappingLayer
			const statementMappingLayer = await db.preparePromisified(`SELECT * FROM \"TUCAN6_HDI_DB_TUCAN_6_1\".\"MappingLayer\";`);
			const resultsMappingLayer = await db.statementExecPromisified(statementMappingLayer, []);
			
			
			//get SourceTableLayer
			const statementSourceTableLayer = await db.preparePromisified(`SELECT * FROM \"TUCAN6_HDI_DB_TUCAN_6_1\".\"SourceTableLayer\";`);
			const resultsSourceTableLayer = await db.statementExecPromisified(statementSourceTableLayer, []);
			
			
			//let result = JSON.stringify({Objects: results});
			return res.render('var6', {
								jsonJoinLayer: resultsSourceTableLayer,
								jsonMappingLayer: resultsMappingLayer,
								jsonSourceTableLayer: resultsSourceTableLayer
							});
		} catch (e) {
			return res.type("text/plain").status(500).send(`ERROR: ${e.toString()}`);
		}
	});
	
	
	//await (even if IDE identifies an error this is correct js code)
	app.get("/await", async(req, res) => {
		try {
			const dbClass = require(global.__base + "utils/dbPromises");
			let db = new dbClass(req.db);
			const statement = await db.preparePromisified(`SELECT SESSION_USER, CURRENT_SCHEMA 
				            								 FROM "DUMMY"`);
			const results = await db.statementExecPromisified(statement, []);
			const statement2 = await db.preparePromisified(`SELECT SESSION_USER, CURRENT_SCHEMA 
				            								 FROM "DUMMY"`);
			const results2 = await db.statementExecPromisified(statement2, []);
			
			
			let result = JSON.stringify({
				Objects: results,
				Objects2: results2
			});
			return res.type("application/json").status(200).send(result);
		} catch (e) {
			return res.type("text/plain").status(500).send(`ERROR: ${e.toString()}`);
		}
	});
	
	
	

	
	
	//Variable test 5 - testing two queries
	app.get("/var5", (req, res) => {
		let client = req.db;
		client.prepare(
			`
			SELECT * FROM \"TUCAN6_HDI_DB_TUCAN_6_1\".\"SourceTableLayer\";
			
			`,
			(err, statement) => {
				if (err) {
					return res.type("text/plain").status(500).send(`ERROR: ${err.toString()}`);
				}
				statement.exec([],
					(err, results) => {
						if (err) {
							return res.type("text/plain").status(500).send(`ERROR: ${err.toString()}`);
						} else {
							var result = JSON.stringify({
								Objects: results
							});
							//return res.type("application/json").status(200).send(results);
							
							return res.render('var5', {
								mappingLayer_json: results
							});
							
						}
					});
				return null;
			});
		return null;
	});
	
	//HDBFUNCTION 1
	app.get("/h1", (req, res) => {
		let client = req.db;
		client.prepare(
			`SELECT * FROM \"TUCAN6_HDI_DB_TUCAN_6_1\".\"MappingLayer\";`,
			(err, statement) => {
				if (err) {
					return res.type("text/plain").status(500).send(`ERROR: ${err.toString()}`);
				}
				statement.exec([],
					(err, results) => {
						if (err) {
							return res.type("text/plain").status(500).send(`ERROR: ${err.toString()}`);
						} else {
							var result = JSON.stringify({
								Objects: results
							});
							return res.render('hdbfunction1', {
			sqlQuery: results
		});
						}
					});
				return null;
			});
		return null;
	});
	
	//Variable 3 test sql
	app.get("/var3", (req, res) => {
		let client = req.db;
		client.prepare(
			`SELECT * FROM \"TUCAN6_HDI_DB_TUCAN_6_1\".\"MappingLayer\";`,
			(err, statement) => {
				if (err) {
					return res.type("text/plain").status(500).send(`ERROR: ${err.toString()}`);
				}
				statement.exec([],
					(err, results) => {
						if (err) {
							return res.type("text/plain").status(500).send(`ERROR: ${err.toString()}`);
						} else {
							var result = JSON.stringify({
								Objects: results
							});
							return res.render('var3', {
			sqlQuery: results
		});
						}
					});
				return null;
			});
		return null;
	});
	
	//Variable 2 test
	app.get("/var2", (req, res) => {
		res.render('var2', {
			myArray:  {
						  "name":"John",
						  "age":30,
						  "cars": {
						    "car1":"Ford",
						    "car2":"BMW",
						    "car3":"Fiat"
						  }
						 } 
		});
	});
	
	//Variable 1 test
	app.get("/var1", (req, res) => {
		res.render('variable1', {
			myVariable: 'Meine Variable'
		});
	});
	
	
	//Download2 test
	app.get("/dl2", (req, res) => {
		res.render('download2');
	});
	
	//Download1 test
	app.get("/dl", (req, res) => {
		res.render('download1');
	});
	
	//Form 1 get
	app.get("/1", (req, res) => {
		res.render('form1');
	});
	
	//Form 1 post
	app.post("/1abgeschickt", (req, res) => {
		console.log('Submitted');
		var ergebnisHTML = '<p>Daten erhalten. Vielen Dank</p><p>' + 
		'[1]= ' + req.body.dynamicMappingParameter1 +'</p><p>'+
		'[2]= ' + req.body.dynamicMappingParameter2 +'</p><p>'+
		'[3]= ' + req.body.dynamicMappingParameter3 +'</p>';
		
		var ergebnisString = 'Daten erhalten. Vielen Dank\r\n' + 
		'[1]= ' + req.body.dynamicMappingParameter1 +'\r\n'+
		'[2]= ' + req.body.dynamicMappingParameter2 +'\r\n'+
		'[3]= ' + req.body.dynamicMappingParameter3;
		
		ergebnisString = "test";
		
		ergebnisString = 'Daten erhalten. Vielen Dank' + 
		'[1]= ' + req.body.dynamicMappingParameter1 +''+
		'[2]= ' + req.body.dynamicMappingParameter2 +''+
		'[3]= ' + req.body.dynamicMappingParameter3;
		
		var rightNow = new Date();
		var resDate = rightNow.toISOString().slice(0,10).replace(/-/g,"");
		var resTime = rightNow.toLocaleTimeString().replace(/:/g,"").slice(0,5);
		var fileName = resDate + '_' + resTime + '.hdbprocedure';
		console.log(fileName);
		
		
		
		var htmlResult = `
	<script>
	function download(filename, text) {
    var pom = document.createElement('a');
    pom.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    pom.setAttribute('download', filename);

    if (document.createEvent) {
        var event = document.createEvent('MouseEvents');
        event.initEvent('click', true, true);
        pom.dispatchEvent(event);
    }
    else {
        pom.click();
    }
}` +

`download('`+ fileName +`', '`+ ergebnisString +`') </script>;` + ergebnisHTML;
		res.send(htmlResult);
		return;
	});
	

	
	//SQL test 3= mapping layer
		app.get("/3", (req, res) => {
			let client = req.db;
			client.prepare(
				`SELECT * FROM \"TUCAN6_HDI_DB_TUCAN_6_1\".\"MappingLayer\";`,
				(err, statement) => {
					if (err) {
						return res.type("text/plain").status(500).send(`ERROR: ${err.toString()}`);
					}
					statement.exec([],
						(err, results) => {
							if (err) {
								return res.type("text/plain").status(500).send(`ERROR: ${err.toString()}`);
							} else {
								var result = JSON.stringify({
									Objects: results
								});
								return res.type("application/json").status(200).send(result);
							}
						});
					return null;
				});
			return null;
		});
		
		
		app.get("/2", (req, res) => {
			let client = req.db;
			client.prepare(
				`SELECT * FROM \"TUCAN6_HDI_DB_TUCAN_6_1\".\"JoinLayer\";`,
				(err, statement) => {
					if (err) {
						return res.type("text/plain").status(500).send(`ERROR: ${err.toString()}`);
					}
					statement.exec([],
						(err, results) => {
							if (err) {
								return res.type("text/plain").status(500).send(`ERROR: ${err.toString()}`);
							} else {
								var result = JSON.stringify({
									Objects: results
								});
								return res.type("application/json").status(200).send(result);
							}
						});
					return null;
				});
			return null;
		});
		
		
		app.get("/1", (req, res) => {
			let client = req.db;
			client.prepare(
				`SELECT * FROM \"TUCAN6_HDI_DB_TUCAN_6_1\".\"SourceTableLayer\";`,
				(err, statement) => {
					if (err) {
						return res.type("text/plain").status(500).send(`ERROR: ${err.toString()}`);
					}
					statement.exec([],
						(err, results) => {
							if (err) {
								return res.type("text/plain").status(500).send(`ERROR: ${err.toString()}`);
							} else {
								var result = JSON.stringify({
									Objects: results
								});
								return res.type("application/json").status(200).send(result);
							}
						});
					return null;
				});
			return null;
		});
		
		app.get("/df1", (req, res) => {
			let client = req.db;
			client.prepare(
				`SELECT * FROM \"TUCAN6_HDI_DB_TUCAN_6_1\".\"SourceTableLayer\";`,
				(err, statement) => {
					if (err) {
						return res.type("text/plain").status(500).send(`ERROR: ${err.toString()}`);
					}
					statement.exec([],
						(err, results) => {
							if (err) {
								return res.type("text/plain").status(500).send(`ERROR: ${err.toString()}`);
							} else {
								//var result = JSON.stringify({Objects: results});
								var DataFrame = require('dataframe-js').DataFrame;
								//var df = "[" + results + "]";
								var helperArray = results;
								var df = new DataFrame(helperArray);

								//var df2 = new DataFrame([{c1: 1, c2: 6}, {c4: 1, c3: 2}], ['c1', 'c2', 'c3', 'c4']);
								return res.type("application/json").status(200).send(df.show());
							}
						});
					return null;
				});
			return null;
		});
		
		
		
		
		
	return app;
};