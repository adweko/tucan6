/*eslint-env node, es6 */
"use strict";

module.exports = (app, server) => {
	app.use("/node", require("./sascha")());
	app.use("/node/sqldummy", require("./dummysql")());
	app.use("/node/sqltest", require("./sqltest")());
	app.use("/node/pug", require("./pug")());
	app.use("/node/xls", require("./xls")());
//	app.use("/node/cdsConv", require("./routes/cdsConv")());
//	app.use("/node/myNode", require("./routes/myNode")());


};