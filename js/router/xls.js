/*eslint no-console: 0, no-unused-vars: 0, no-shadow: 0, new-cap: 0*/
/*eslint-env node, es6 */
"use strict";
var express = require("express");
var async = require("async");

module.exports = function() {
	var app = express.Router();
	
	
	//Hallo Router
	app.get("/hallo", (req, res) => {
		res.send("WhatSAP? :)?");
	});
	
	
	//XLSX test
	app.get("/1", (req, res) => {
		
		
		
		var htmlResult = `<script></script><p>hello world</p>`;
		res.send(htmlResult);
		return;
	});
	
	//XLSX test2
	app.get("/2", (req, res) => {
		
		
		
		var htmlResult = `
		
		
<html lang="en">    

<title>Please Upload XLSX or XLS file</title>

<head>
    <script
			  src="https://code.jquery.com/jquery-1.1.1.pack.js"
			  integrity="sha256-aIROcR0yayONO43ZZgadzr177fNOhHlg/FqPXP6qfhI="
			  crossorigin="anonymous"></script>
	<script lang="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.10.8/xlsx.full.min.js">
	</script>


    <script>
        $(document).ready(function(){
              $("#fileUploader").change(function(evt){
                    var selectedFile = evt.target.files[0];
                    var reader = new FileReader();
                    reader.onload = function(event) {
                      var data = event.target.result;
                      var workbook = XLSX.read(data, {
                          type: 'binary'
                      });
					  var result = {};
                      workbook.SheetNames.forEach(function(sheetName) {
						
                          
						  
						  var XL_row_object = XLSX.utils.sheet_to_json(workbook.Sheets[sheetName]);
						  if(XL_row_object.length) result[sheetName] = XL_row_object;
                          //var json_object = JSON.stringify(result, 2, 2);
						  
                          document.getElementById("jsonObject").innerHTML = JSON.stringify(result, 2, 2);

                        })
                    };

                    reader.onerror = function(event) {
                      console.error("File could not be read! Code " + event.target.error.code);
                    };

                    reader.readAsBinaryString(selectedFile);
              });
        });
    </script>

</head>

<body>

    <input type="file" id="fileUploader" name="fileUploader" accept=".xls, .xlsx"/>
    </br></br>
    <label id="jsonObject"> JSON : </label>
</body>
</html>
		
		
		
		`;
		res.send(htmlResult);
		return;
	});

	return app;
};